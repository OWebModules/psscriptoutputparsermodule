﻿using LogModule;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using PsScriptOutputParserModule.Models;
using PsScriptOutputParserModule.Services;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TextParserModule;
using TextParserModule.Models;

namespace PsScriptOutputParserModule
{
    public class PsScriptOutputParserController : AppController
    {
        public async Task<ScriptOutputResult> PsScriptOutput(ScriptOutputRequest request)
        {
            var taskResult = await Task.Run<ScriptOutputResult>(async () =>
            {
                var result = new ScriptOutputResult
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var parseStamp = DateTime.Now;
                var serviceAgent = new ElasticAgent(Globals);

                request.MessageOutput?.OutputInfo($"Get model...");
                var getModelResult = await serviceAgent.GetModel(request);
                request.MessageOutput?.OutputInfo($"Have model.");

                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var textParserController = new TextParserController(Globals);

                if (getModelResult.ScriptOutputToTextParser != null)
                {


                    var oTextParser = new clsOntologyItem
                    {
                        GUID = getModelResult.ScriptOutputToTextParser.ID_Other,
                        Name = getModelResult.ScriptOutputToTextParser.Name_Other,
                        GUID_Parent = getModelResult.ScriptOutputToTextParser.ID_Parent_Other,
                        Type = getModelResult.ScriptOutputToTextParser.Ontology
                    };
                    request.MessageOutput?.OutputInfo($"Get Textparser...");
                    var textParserResult = await textParserController.GetTextParser(oTextParser);

                    result.ResultState = textParserResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Textparser";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    if (textParserResult.Result.Count != 1)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "There is no Textparser!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    
                    result.TextParser = textParserResult.Result.First();
                    request.MessageOutput?.OutputInfo($"Have Textparser: {result.TextParser.NameParser}");

                    request.MessageOutput?.OutputInfo($"Get Textparserfields...");
                    var textParserFieldResult = await textParserController.GetParserFields(new clsOntologyItem { GUID = result.TextParser.IdFieldExtractor, Name = result.TextParser.NameFieldExtractor, GUID_Parent = result.TextParser.IdClassFieldExtractor, Type = Globals.Type_Object });

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Textparser Fields";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    if (!textParserFieldResult.Result.Any())
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "The Textparser has no Entry-Fields!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;

                    }
                    
                    result.TextParserFields = textParserFieldResult.Result;
                    request.MessageOutput?.OutputInfo($"Have {result.TextParserFields.Count} Textparserfields.");
                }

                var commandLineRunController = new CommandLineRunModule.CommandLineRunController(Globals);

                if (getModelResult.ScriptOutputToCommandLineRunControl != null)
                {

                    request.MessageOutput?.OutputInfo($"Get Control-Script...");
                    var scriptResult = await commandLineRunController.GetCommandLineRun(new clsOntologyItem
                    {
                        GUID = getModelResult.ScriptOutputToCommandLineRunControl.ID_Other,
                        Name = getModelResult.ScriptOutputToCommandLineRunControl.Name_Other,
                        GUID_Parent = getModelResult.ScriptOutputToCommandLineRunControl.ID_Parent_Other,
                        Type = getModelResult.ScriptOutputToCommandLineRunControl.Ontology
                    });

                    result.ResultState = scriptResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Control-Script";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    if (scriptResult.Result == null || string.IsNullOrEmpty(scriptResult.Result.CodeParsed))
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "The Control-Script is not valid!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;

                    }

                    result.ControlScript = scriptResult.Result;
                    request.MessageOutput?.OutputInfo($"Have Control-Script: {result.ControlScript.Name_CommandLineRun}");
                }

                request.MessageOutput?.OutputInfo($"Get Data-Script...");
                var scriptResultData = await commandLineRunController.GetCommandLineRun(new clsOntologyItem
                {
                    GUID = getModelResult.ScriptOutputToCommandLineRunData.ID_Other,
                    Name = getModelResult.ScriptOutputToCommandLineRunData.Name_Other,
                    GUID_Parent = getModelResult.ScriptOutputToCommandLineRunData.ID_Parent_Other,
                    Type = getModelResult.ScriptOutputToCommandLineRunData.Ontology
                });

                result.ResultState = scriptResultData.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Data-Script";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (scriptResultData.Result == null || string.IsNullOrEmpty(scriptResultData.Result.CodeParsed))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The Data-Script is not valid!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;

                }

                result.DataScript = scriptResultData.Result;

                request.MessageOutput?.OutputInfo($"Have Data-Script: {result.DataScript.Name_CommandLineRun}");

                request.MessageOutput?.OutputInfo($"Use {getModelResult.ScriptOutputToHosts.Count} Hosts");
                if (getModelResult.ScriptOutputToHosts.Any())
                {
                    result.Hosts = getModelResult.ScriptOutputToHosts.Select(rel => new HostResult
                    {
                        ResultState = Globals.LState_Success.Clone(),
                        Host = new clsOntologyItem
                        {
                            GUID = rel.ID_Other,
                            Name = rel.Name_Other,
                            GUID_Parent = rel.ID_Parent_Other,
                            Type = rel.Ontology
                        }
                    }).ToList();
                }


                result.User = getModelResult.ScriptOutputToUser != null ? new clsOntologyItem
                {
                    GUID = getModelResult.ScriptOutputToUser.ID_Other,
                    Name = getModelResult.ScriptOutputToUser.Name_Other,
                    GUID_Parent = getModelResult.ScriptOutputToUser.ID_Parent_Other,
                    Type = getModelResult.ScriptOutputToUser.Ontology
                } : null;

                result.Path = new clsOntologyItem
                {
                    GUID = getModelResult.ScriptOutputToWorkPath.ID_Other,
                    Name = getModelResult.ScriptOutputToWorkPath.Name_Other,
                    GUID_Parent = getModelResult.ScriptOutputToWorkPath.ID_Parent_Other,
                    Type = getModelResult.ScriptOutputToWorkPath.Ontology
                };

                var securityController = new SecurityController(Globals);

                string password = "";
                SecurityModule.Services.CredentialItem loginCredentialItem = null;

                if (!string.IsNullOrEmpty(request.IdMasterUser) && !string.IsNullOrEmpty(request.MasterPassword))
                {
                    var loginCredentials = await securityController.LoginWithUser(request.IdMasterUser, request.MasterPassword);

                    if (loginCredentials.Result.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "Login not Successflul";
                        return result;
                    }

                    loginCredentialItem = loginCredentials.CredentialItems.FirstOrDefault();
                }

                if (result.User != null)
                {
                    

                    var getPasswordResult = await securityController.GetPassword(result.User, loginCredentialItem.Password.Name_Other);

                    result.ResultState = getPasswordResult.Result;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = $"Error while getting Password for user {result.User.Name}";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    password = getPasswordResult.CredentialItems.Any() ? getPasswordResult.CredentialItems.First().Password.Name_Other : "";
                }
                
                try
                {
                    if (Directory.Exists(result.ScriptPath))
                    {
                        Directory.Delete(result.ScriptPath, true);
                    }

                    Directory.CreateDirectory(result.ScriptPath);


                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Create Folder: {ex.Message}";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                

                try
                {
                    request.MessageOutput?.OutputInfo($"Save DataScript...");
                    File.WriteAllText(result.DataScriptPath, result.DataScript.CodeParsed, Encoding.Unicode);
                    request.MessageOutput?.OutputInfo($"Saved DataScript.");
                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Create Data-Script: {ex.Message}";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (getModelResult.ScriptOutputToCommandLineRunControl != null)
                {
                    result.ControlScript.CodeParsed = result.ControlScript.CodeParsed.
                        Replace($"@{Config.LocalData.Object_SCRIPT_FULLPATH.Name}@", result.DataScriptPath);
                }

                var scriptCode = result.DataScript.CodeParsed;
                if (result.User != null)
                {
                    if (scriptCode.Contains($"@{Config.LocalData.Object_USERNAME.Name}@"))
                    {
                        scriptCode = scriptCode.Replace($"@{Config.LocalData.Object_USERNAME.Name}@", result.User.Name);
                    }
                    if (scriptCode.Contains($"@{Config.LocalData.Object_PASSWORD.Name}@"))
                    {
                        scriptCode = scriptCode.Replace($"@{Config.LocalData.Object_PASSWORD.Name}@", password);
                    }
                }

                if (!string.IsNullOrEmpty(request.IdMasterUser) && !string.IsNullOrEmpty(request.MasterPassword))
                {
                    if (scriptCode.Contains($"@{Config.LocalData.Object_MASTERUSERNAME.Name}@"))
                    {
                        scriptCode = scriptCode.Replace($"@{Config.LocalData.Object_MASTERUSERNAME.Name}@", loginCredentialItem.User.Name);
                    }
                    if (scriptCode.Contains($"@{Config.LocalData.Object_MASTERPASSWORD.Name}@"))
                    {
                        scriptCode = scriptCode.Replace($"@{Config.LocalData.Object_MASTERPASSWORD.Name}@", loginCredentialItem.Password.Name_Other);
                    }
                    if (scriptCode.Contains($"@{Config.LocalData.Object_MASTERUSEREMAIL.Name}@"))
                    {
                        scriptCode = scriptCode.Replace($"@{Config.LocalData.Object_MASTERUSEREMAIL.Name}@", loginCredentialItem.EmailAddress.Name);
                    }
                }
                    
                try
                {
                    request.MessageOutput?.OutputInfo($"Save ControlScript...");
                    File.WriteAllText(result.DataScriptPath, scriptCode, Encoding.Unicode);
                    request.MessageOutput?.OutputInfo($"Saved ControlScript.");
                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Create Data-Script: {ex.Message}";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var arguments = "";

                if (!string.IsNullOrEmpty(result.ControlScriptPath) && result.Hosts.Any())
                {
                    arguments = result.ControlScriptPath;
                }
                else
                {
                    arguments = result.DataScriptPath;
                }

                if (result.User != null)
                {
                    arguments = $"{arguments} '{result.User.Name}'";
                }

                if (!string.IsNullOrEmpty(password))
                {
                    arguments = $"{arguments} '{password}'";
                }

                arguments = $"\"{arguments}\"";

                var logController = new LogController(Globals);

                request.MessageOutput?.OutputInfo($"Get last orderid of logentry...");
                var lastOrderIdResult = await serviceAgent.GetLastOrderIdParseStamp(getModelResult.ScriptOutput.GUID, logController.ClassLogEntry);
                result.ResultState = lastOrderIdResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Have last orderid of logentry.");

                request.MessageOutput?.OutputInfo($"Save Logentry...");
                var orderId = lastOrderIdResult.Result + 1;
                var logEntryResult = await logController.CreateLogEntry(Globals.LState_Success, getModelResult.ScriptOutput, result?.User ?? loginCredentialItem.User, "Parsed PS-Script", parseStamp, orderId);

                result.ResultState = logEntryResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Saved Logentry.");

                if (result.Hosts.Any())
                {
                    foreach (var host in result.Hosts)
                    {
                        request.MessageOutput?.OutputInfo($"Execute Script on {host.Host.Name}...");
                        var exeRequest = new ExecuteCodeRequest(!string.IsNullOrEmpty(result.ControlScriptPath) ? result.ControlScriptPath : result.DataScriptPath,
                            result.ControlScript != null ? result.ControlScript.CodeParsed : result.DataScript.CodeParsed, arguments, result.ScriptPath)
                        {
                            HostName = host.Host.Name
                        };

                        var exeResult = await Execute(exeRequest);


                        if (exeResult.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            if (exeResult.Result.CriticalErrorOccured)
                            {
                                host.ResultState = exeResult.ResultState;
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                                
                            }
                            else
                            {
                                result.ResultState = exeResult.ResultState;
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                continue;
                            }
                        }

                        if (getModelResult.ScriptOutputToTextParser != null)
                        {
                            result.ResultState = await ParseOutput(result.TextParserFields,
                                result.TextParser,
                                exeResult.Result.ExecutionOutput,
                                request.CancellationToken,
                                parseStamp,
                                textParserController,
                                host.Host.Name,
                                exeResult.Result.ErrorOutput,
                                logEntryResult.Result.IdLogEntry);
                        }
                        else
                        {
                            result.Output += $"Host: {host.Host.Name}\n{exeResult.Result.ExecutionOutput}\n";
                        }
                        request.MessageOutput?.OutputInfo($"Executed Script on {host.Host.Name}");
                    }
                }
                else
                {
                    request.MessageOutput?.OutputInfo($"Execute Script on localhost...");
                    var exeRequest = new ExecuteCodeRequest(result.DataScriptPath,
                            result.DataScript.CodeParsed, arguments, result.ScriptPath);
                    var exeResult = await Execute(exeRequest);


                    if (exeResult.ResultState.GUID == Globals.LState_Error.GUID)
                    {

                        result.ResultState = exeResult.ResultState;
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;

                    }



                    

                    if (getModelResult.ScriptOutputToTextParser != null)
                    {
                        result.ResultState = await ParseOutput(result.TextParserFields,
                                result.TextParser,
                                exeResult.Result.ExecutionOutput,
                                request.CancellationToken,
                                parseStamp,
                                textParserController,
                                Environment.MachineName,
                                exeResult.Result.ErrorOutput,
                                logEntryResult.Result.IdLogEntry);
                    }
                    else
                    {
                        result.Output = exeResult.Result.ExecutionOutput.ToString();
                    }
                    

                    request.MessageOutput?.OutputInfo($"Executed Script on localhost.");
                }

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                

                return result;
            });
            return taskResult;
        }

        private async Task<clsOntologyItem> ParseOutput(List<ParserField> textParserFields,
            TextParser textParser,
            StringBuilder executionOutput,
            CancellationToken cancellationToken,
            DateTime parseStamp,
            TextParserController textParserController,
            string hostName,
            StringBuilder error,
            string logEntryId)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var parseTextRequest = new ParseTextRequest
                {
                    ReplaceNewLine = true,
                    FileLine = 0,
                    ParserFields = textParserFields,
                    TextParser = textParser,
                    Text = executionOutput.ToString(),
                    CancellationToken = cancellationToken


                };

                var hostField = textParserFields.FirstOrDefault(field => field.NameField == "Host");

                if (hostField == null)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No Host-Field provided!";
                    return result;
                }

                hostField.CustomContent = hostName;

                var parseStampField = textParserFields.FirstOrDefault(field => field.NameField == "ParseStamp");
                if (parseStampField == null)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No ParseStamp-Field provided!";
                    return result;
                }

                parseStampField.CustomContent = parseStamp;


                var errorField = textParserFields.FirstOrDefault(field => field.NameField == "Error");
                if (errorField == null)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No Error-Field provided!";
                    return result;
                }

                if (!string.IsNullOrEmpty(error.ToString()))
                {

                    errorField.CustomContent = error.ToString();
                }
                else
                {
                    errorField.CustomContent = "";
                }

                var logIxField = textParserFields.FirstOrDefault(field => field.NameField == "LogId");
                if (logIxField == null)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No LogId-Field provided!";
                    return result;
                }

                logIxField.CustomContent = logEntryId;
                
                var parseResult = await textParserController.ParseWholeText(parseTextRequest);

                result = parseResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }


                return result;
            });

            return taskResult;

        }

        private async Task<ResultItem<ExecuteCodeResult>> Execute(ExecuteCodeRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ExecuteCodeResult>>(() =>
            {
                
                var result = new ResultItem<ExecuteCodeResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ExecuteCodeResult()
                };

                try
                {
                    File.WriteAllText(request.ScriptToExecute, request.ParsedCode.Replace($"@{Config.LocalData.Object_HOST.Name}@", request.HostName), Encoding.Unicode);
                }
                catch (Exception ex)
                {
                    result.Result.CriticalErrorOccured = true;
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Create Control-Script: {ex.Message}";
                    return result;
                }
                Process process = new Process();
                process.StartInfo.FileName = "powershell.exe";
                process.StartInfo.Arguments = request.Arguments;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.WorkingDirectory = request.WorkingDirectory;
                process.Start();
                //* Read the output (or the error)
                while (!process.StandardOutput.EndOfStream)
                {
                    result.Result.ExecutionOutput.AppendLine(process.StandardOutput.ReadLine());
                }

                while (!process.StandardError.EndOfStream)
                {
                    result.Result.ErrorOutput.AppendLine(process.StandardError.ReadToEnd());
                }

                process.WaitForExit();



                if (!string.IsNullOrEmpty(result.Result.ErrorOutput.ToString()))
                {


                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Script-Error: {result.Result.ErrorOutput.ToString()}";
                    return result;

                }


                result.ResultState = DeleteFile(request.ScriptToExecute);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;

        }

        public async Task<ResultOItem<ClassObject>> GetClassObjectReference(clsOntologyItem refItem)
        {
            var serviceElastic = new ElasticAgent(Globals);

            var reference = await serviceElastic.GetClassObject(refItem);

            if (reference.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return reference;
            }

            if (reference.Result.ObjectItem.GUID_Parent != Config.LocalData.Class_PsScriptOutputParser.GUID)
            {
                reference.ResultState = Globals.LState_Nothing.Clone();
                reference.ResultState.Additional1 = "The Reference is no Script Output";
            }

            return reference;
        }

        private clsOntologyItem DeleteFile(string filePath)
        {
            var result = Globals.LState_Success.Clone();
            try
            {
                File.Delete(filePath);
            }
            catch (Exception ex)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = $"Error while deleting of file {ex.Message}";
                return result;

            }
            return result;
        }

        public PsScriptOutputParserController(Globals globals) : base(globals)
        {
        }


    }
}
