﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PsScriptOutputParserModule.Models
{
    public class ScriptOutputRequest
    {
        public string IdScriptOutput { get; private set; }
        public string IdMasterUser { get; private set; }
        public string MasterPassword { get; private set; }
        public CancellationToken CancellationToken { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public ScriptOutputRequest(string idScriptOutput, string idMasterUser, string masterPassword, CancellationToken cancellationToken)
        {
            IdScriptOutput = idScriptOutput;
            IdMasterUser = idMasterUser;
            MasterPassword = masterPassword;
            CancellationToken = cancellationToken;
        }
    }
}
