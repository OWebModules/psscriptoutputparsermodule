﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsScriptOutputParserModule.Models
{
    public class ExecuteCodeRequest
    {
        public string ScriptToExecute { get; private set; }
        public string ParsedCode { get; private set; }
        public string Arguments { get; private set; }
        public string WorkingDirectory { get; private set; }
        public string HostName { get; set; }

        public ExecuteCodeRequest(string scriptToExecute, string parsedCode, string arguments, string workingDirectory)
        {
            ScriptToExecute = scriptToExecute;
            ParsedCode = parsedCode;
            Arguments = arguments;
            WorkingDirectory = workingDirectory;
        }
    }
}
