﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsScriptOutputParserModule.Models
{
    public class ExecuteCodeResult
    {
        public bool CriticalErrorOccured { get; set; }
        public StringBuilder ErrorOutput { get; set; } = new StringBuilder();
        public StringBuilder ExecutionOutput { get; set; } = new StringBuilder();
    }
}
