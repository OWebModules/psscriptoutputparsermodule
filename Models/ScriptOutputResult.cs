﻿using CommandLineRunModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace PsScriptOutputParserModule.Models
{
    public class ScriptOutputResult
    {
        public clsOntologyItem ResultState { get; set; }

        public TextParser TextParser { get; set; }

        public List<ParserField> TextParserFields { get; set; } = new List<ParserField>();

        public CommandLineRun ControlScript { get; set; }
        public string ControlScriptPath
        {
            get
            {
                if (string.IsNullOrEmpty(ScriptPath) || ControlScript == null) return null;

                return System.IO.Path.Combine(ScriptPath, "Control.ps1");
            }
        }

        public CommandLineRun DataScript { get; set; }

        public string DataScriptPath
        {
            get
            {
                if (string.IsNullOrEmpty(ScriptPath)) return null;

                return System.IO.Path.Combine(ScriptPath, "Data.ps1");
            }
        }
        public clsOntologyItem Path { get; set; }

        public string ScriptPath
        {
            get
            {
                return Path != null ? Environment.ExpandEnvironmentVariables(Path.Name) : null;
            }
        }

        public List<HostResult> Hosts { get; set; } = new List<HostResult>();

        public clsOntologyItem User { get; set; }

        public string Output { get; set; }

    }

    public class HostResult
    {
        public clsOntologyItem ResultState { get; set; }
        public clsOntologyItem Host { get; set; }
    }
}
