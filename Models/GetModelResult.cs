﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsScriptOutputParserModule.Models
{
    public class GetModelResult
    {
        public clsOntologyItem ScriptOutput { get; set; }
        public clsObjectRel ScriptOutputToCommandLineRunData { get; set; }
        public clsObjectRel ScriptOutputToCommandLineRunControl { get; set; }

        public List<clsObjectRel> ScriptOutputToHosts { get; set; }

        public clsObjectRel ScriptOutputToWorkPath { get; set; }
        public clsObjectRel ScriptOutputToTextParser { get; set; }
        public clsObjectRel ScriptOutputToUser { get; set; }

        public clsOntologyItem ResultState { get; set; }
    }
}
