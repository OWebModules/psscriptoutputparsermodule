﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using PsScriptOutputParserModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsScriptOutputParserModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {

        public async Task<GetModelResult> GetModel(ScriptOutputRequest request)
        {
            var taskResult = await Task.Run<GetModelResult>(async () =>
            {
                var result = new GetModelResult
                {
                    ResultState = globals.LState_Success.Clone()
                };

                if (string.IsNullOrEmpty(request.IdScriptOutput) || !globals.is_GUID(request.IdScriptOutput))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Invalid Id of ScriptOutput!";
                    return result;
                }

                var resultOItem = await GetOItem(request.IdScriptOutput, globals.Type_Object);

                result.ResultState = resultOItem.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "ScriptOutput cannot be found!";
                    return result;
                }

                result.ScriptOutput = resultOItem.Result;

                var searchDataScripts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdScriptOutput,
                        ID_RelationType = Config.LocalData.ClassRel_PsScriptOutputParser_Data_Script_Comand_Line__Run_.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_PsScriptOutputParser_Data_Script_Comand_Line__Run_.ID_Class_Right
                    }
                };

                var dbReaderDataScripts = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderDataScripts.GetDataObjectRel(searchDataScripts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading DataScripts";
                    return result;
                }

                if (dbReaderDataScripts.ObjectRels.Count != 1)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "You have to relate exact one Data-Script";
                    return result;
                }

                result.ScriptOutputToCommandLineRunData = dbReaderDataScripts.ObjectRels.First();

                var searchControlScripts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdScriptOutput,
                        ID_RelationType = Config.LocalData.ClassRel_PsScriptOutputParser_Control_Script_Comand_Line__Run_.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_PsScriptOutputParser_Control_Script_Comand_Line__Run_.ID_Class_Right
                    }
                };

                var dbReaderControlScripts = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderControlScripts.GetDataObjectRel(searchControlScripts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading ControlScripts";
                    return result;
                }
                if (dbReaderControlScripts.ObjectRels.Any())
                {
                    result.ScriptOutputToCommandLineRunControl = dbReaderControlScripts.ObjectRels.First();
                }


                var searchScriptOutputToHosts = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdScriptOutput,
                        ID_RelationType = Config.LocalData.ClassRel_PsScriptOutputParser_uses_Host.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_PsScriptOutputParser_uses_Host.ID_Class_Right
                    }
                };

                var dbReaderHosts = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderHosts.GetDataObjectRel(searchScriptOutputToHosts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Hosts";
                    return result;
                }

                result.ScriptOutputToHosts = dbReaderHosts.ObjectRels;

                var searchTextParsers = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdScriptOutput,
                        ID_RelationType = Config.LocalData.ClassRel_PsScriptOutputParser_uses_Textparser.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_PsScriptOutputParser_uses_Textparser.ID_Class_Right
                    }
                };

                var dbReaderTextParsers = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextParsers.GetDataObjectRel(searchTextParsers);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Textparsers";
                    return result;
                }

                if (dbReaderTextParsers.ObjectRels.Any())
                {
                    result.ScriptOutputToTextParser = dbReaderTextParsers.ObjectRels.First();
                }


                var searchPaths = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdScriptOutput,
                        ID_RelationType = Config.LocalData.ClassRel_PsScriptOutputParser_work_in_Path.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_PsScriptOutputParser_work_in_Path.ID_Class_Right
                    }
                };

                var dbReaderPaths = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Paths";
                    return result;
                }

                if (dbReaderPaths.ObjectRels.Count != 1)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "You have to relate a Path to the ScriptOutput";
                    return result;
                }

                result.ScriptOutputToWorkPath = dbReaderPaths.ObjectRels.First();

                var searchUsers = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdScriptOutput,
                        ID_RelationType = Config.LocalData.ClassRel_PsScriptOutputParser_authorized_by_user.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_PsScriptOutputParser_authorized_by_user.ID_Class_Right
                    }
                };

                var dbReaderUsers = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUsers.GetDataObjectRel(searchUsers);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Users";
                    return result;
                }

                result.ScriptOutputToUser = dbReaderUsers.ObjectRels.FirstOrDefault();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<long>> GetLastOrderIdParseStamp(string idConfig, clsOntologyItem classLogEntry)
        {
            var taskResult = await Task.Run<ResultItem<long>>(() =>
           {
               var result = new ResultItem<long>
               {
                   ResultState = globals.LState_Success.Clone()
               };

               var dbReader = new OntologyModDBConnector(globals);

               
               result.Result = dbReader.GetDataRelOrderId(new clsOntologyItem { GUID_Parent = classLogEntry.GUID }, new clsOntologyItem { GUID = idConfig }, globals.RelationType_belongsTo, false);

               return result;
           });

            return taskResult;
        }
        
        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
